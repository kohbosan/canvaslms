from setuptools import setup, find_packages

setup(
    name='CanvasLMS',
    version='0.1',
    packages=find_packages(),
    url='',
    license='',
    author='Juan Menendez',
    author_email='menendezjuanj@gmail.com',
    description='Provides Python bindings to the Canvas LMS API.'
)
