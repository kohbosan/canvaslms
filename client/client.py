from requests import request


class CanvasException(Exception):
    pass


class Client(object):
    def __init__(self, api_key=None):
        super(object, Client)
        self.api_key = api_key if api_key else self.getapikey()

    def getapikey(self):
        print("getting api key")

    def _apicall(self, endpoint, method="get", **kwargs):
        baseurl = "https://canvas.instructure.com/api/v1/"
        assert method in ["get", "post"]
        r = request(method, baseurl + endpoint, **kwargs)
        return r.json()

    @property
    def assignments(self, **kwargs):
